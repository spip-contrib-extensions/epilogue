<?php
/**
 * Fonctions utiles au plugin epilogue
 *
 * @plugin     epilogue
 * @copyright  2018
 * @author     Amaury Adon
 * @licence    GNU/GPL
 * @package    SPIP\Epilogue\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
