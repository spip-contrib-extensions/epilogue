<?php
/**
 * Utilisations de pipelines par epilogue
 *
 * @plugin     epilogue
 * @copyright  2018
 * @author     Amaury Adon
 * @licence    GNU/GPL
 * @package    SPIP\Epilogue\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
