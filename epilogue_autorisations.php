<?php
/**
 * Définit les autorisations du plugin epilogue
 *
 * @plugin     epilogue
 * @copyright  2018
 * @author     Amaury Adon
 * @licence    GNU/GPL
 * @package    SPIP\Epilogue\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function epilogue_autoriser() {
}
