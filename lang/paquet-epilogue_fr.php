<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'epilogue_description' => 'Portage sur SPIP du modèle Epilogue de Templated.co (https://templated.co/epilogue). Une page d\'atterrissage lisse et minimaliste avec des lignes pures. Une prise en compte des articles et des rubriques SPIP a été ajoutée ainsi qu\'un menu à droite.',
	'epilogue_nom' => 'Épilogue',
	'epilogue_slogan' => 'Squelette basé sur Epilogue ',
);
